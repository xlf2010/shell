#!/bin/bash

[ "$#" -ne "2" ] && echo "please input the project folder and branch name" && exit 1

#Auto Merge
GIT_MERGE_AUTOEDIT=no
export GIT_MERGE_AUTOEDIT

basdir=`/d/code/work/git/yyfax_public`
cd $basdir/$1
branch="$2"
remote_rep="origin"
cf="/tmp/conflict_file.$1"
[ -f ${cf} ] && rm -f ${cf}

curr_branch=`git branch | grep '*'`

# checkout branch
function chkout (){
	br="$1"
	remote_br="$2"
	git checkout $br
	if [ "$?" -ne "0" ] 
	then 
		git checkout -b $br $remote_br
		[ "$?" -ne "0" ] && echo "branch $br doesn't exist" && exit 1
	fi
}

#handle conflit, commit conflict file
function handle_conflit (){
	br="$1"
	using="$2"
	echo "merge master to branch :$br has conflict,file list:" >> ${cf}
	conflit_cnt=`git status | grep both | wc -l`
	[ "$conflit_cnt" -eq "0" ] && return
	git status | grep both | awk '{print $3}' | while read f
	do 
#		git checkout --${using} ${f}
		git add ${f}
		echo "${f}" >> ${cf}		
	done
	git commit -m "conflict while merge master to branch: $br"
}

# update master
git checkout master && git pull

# update branch
chkout $branch ${remote_rep}/$branch
git pull
#switch master and pull
git checkout master
git merge $branch
# handle conflict 
[ "$?" -ne "0" ] && echo "branch $branch merge to master conflict exist ,exit" && exit 1

#check if the ${branch} is same as master
res=`git diff ${branch} master`
if [ -n "$res"  ] ; then
	echo "branch ${branch} is diff from master,please use `git diff ${branch} master` to check it "
	exit 1
fi

# push master to remote
git push

# create and push tag
git checkout $branch && git tag ${branch}-tag && git push --tags

#merge master to other branch
git remote update ${remote_rep} --prune
for remote_br in `git branch -r | grep "${remote_rep}" | grep -Ev "master|${branch}"`
do
	local_br=`echo -n "${remote_br}" | awk -F'/' '{print $2}'`
	chkout $local_br $remote_br
	git pull
	git merge master 
	# handle conflict 
	[ "$?" -ne "0" ] && handle_conflit $local_br "theirs"
	git push
done

# print conflict file list
test -f ${cf} && cat ${cf}

curr_branch=${curr_branch:2}
git checkout "$curr_branch"

#end: please delete remote branch on web

