#/bin/sh

# delete redundancy local branch, sync remote branch 

basdir=`pwd`
cd $basdir

pro_dir="$1"
#cd $pro_dir

#remote_rep="public"


function sync_branch(){
	remote_rep="$1"
	exist_repo="0"
	for i in `git remote` 
	do
		if [ "$remote_rep" = "$i" ];then
			exist_repo="1"
			break
		fi 
	done
	[ "$exist_repo" -eq "0" ] && echo "$2,remote ${remote_rep} doesn't exist... no pull" && return
	curr_branch=`git branch | grep '*'`
	
	git checkout master
	declare -A remote_br=()
	git remote update ${remote_rep} --prune
	for br in `git branch -r | grep "${remote_rep}" | grep -Ev 'master|HEAD'`
	do
		br_name=`echo "$br" | awk -F'/' '{print $2}'`
		remote_br["${br_name}"]="$br_name"
	done

	git branch | grep -Ev 'master|HEAD' | while read br
	do
		br_name=${remote_br["${br}"]}
		if [ "x${br_name}" = "x" ]
		then
			echo "deleting branch $br"
			git branch -D ${br}
		fi
	done
	curr_branch=${curr_branch:2}
	git checkout "$curr_branch"
}

function sync(){
	repo="$1"
	for i in `ls` ; do
		cd $basdir
		if [ ! -d "$i" -o "$i" = "yyfax_public" ]
		then
			continue
		fi
		echo "sync branch $i"
		cd $i
		sync_branch ${repo}
	#	sync_branch "public"
	done
#	cd yyfax-settle 
#	sync_branch ${repo}
}

if [ "$#" -eq "1" ] ; then
	sync "$1"
	exit 0
fi

sync "origin"
sync "public"
