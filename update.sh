#/bin/sh
basdir=`pwd`
cd $basdir

#Auto Merge
GIT_MERGE_AUTOEDIT=no
export GIT_MERGE_AUTOEDIT

function upd(){
		cd "$1/$2"
		exist_repo="0"
		repo="$3"
		for i in `git remote` 
		do
			if [ "$repo" = "$i" ];then
				exist_repo="1"
				break
			fi 
		done
		[ "$exist_repo" -eq "0" ] && echo "$2,remote ${repo} doesn't exist... no pull" && return
		curr_branch=`git branch | grep '*'| awk '{print $2}'`
		echo "update director .. $2,current repo:${repo} current branch:${curr_branch}" 
		git fetch ${repo}
		git pull --no-rebase "${repo}" ${curr_branch}
}


function upd_all(){
	repo="$1"
	for i in `ls` ; do
		cd $basdir
		if [ ! -d "$i" -o "$i" = "yyfax_public" ] 	
		then
			continue
		fi
		upd $basdir $i $repo
	done

	# pulic	
	for i in `ls` ; do
		cd "$basdir/yyfax_public"
		if [ -d "$i" ]; then
			upd $basdir "yyfax_public/${i}" $repo
		fi
	done
}

if [ $# -eq 1 ]; then 
	if [ "$1" = "origin" -o "$1" = "public" ]
	then
		upd_all $1
	else
		upd $basdir $1 "origin"
	fi
	exit 0
fi

if [ $# -eq 2 ]; then 
	upd $basdir $2 $1
	exit 0
fi

#default update all
upd_all "origin"
upd_all "public"

