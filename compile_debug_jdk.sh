#1. 进入jdk目录
dst_dir="general_debug"
flist="folderlist.txt"
out_dir="jdk_debug"
log_file="log.txt"
home=`pwd`
debug_jar_dir="${home}/jre/lib/endorsed"

test -d ${dst_dir} && rm -rf ${dst_dir}
mkdir ${dst_dir}

echo "extracting src.zip to ${dst_dir}"
unzip -q src.zip -d ${dst_dir}
echo "extracted src.zip to ${dst_dir}"

cd ${dst_dir}
test -f "${flist}" && echo -n "" > ${flist}

echo "find java file in org java javax to file ${flist}"
#for d in "java" "javax" "org" 
for d in `ls`
do 
	find ./${d} -type d >> ${flist}
done

[ ! -d ${out_dir} ] && mkdir ${out_dir}
echo "compiling file to ${out_dir}"
while read f
do
	ls ${f}/*.java > /dev/null 2>&1
	if [ "$?" -eq "0" ] ;then 
		echo "compiling folder ${f}"
		javac -J-Xms16m -J-Xmx1024m -sourcepath ./${f} -cp ../jre/lib/rt.jar -d ${out_dir} -g ${f}/*.java >> ${log_file} 2>&1
	fi
done < ${flist}


echo " create debug jar file"
cd ${out_dir} && jar cf0 rt_debug.jar *

echo "copy rt_debug.jar to dst folder ${debug_jar_dir}"
test -d ${debug_jar_dir} && rm -rf ${debug_jar_dir}
mkdir ${debug_jar_dir}
cp rt_debug.jar ${debug_jar_dir}

# 在eclipse上Window->Preferences->Java->Installed JREs->编辑编译目录的jdk->Add External JARs,选择rt_debug.jar
