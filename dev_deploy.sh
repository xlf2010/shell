#/bin/bash
git pull
cd yylh 
mvn clean package -Dmaven.test.skip=true -settings c:/Users/lenovo/settings.xml
if [ $? -ne "0" ]
then 
	exit 1
fi

host=""
port=""
uname=""
passwd=""
remote_path=""
local_path="target"
jar_name="*.jar"


#delete remote host war 
expect << EOF
    set timeout 3600
    spawn ssh -p ${port} ${uname}@${host}
    expect {
		"*yes/no" { send "yes\n"; exp_continue }
		"*password:" { send "$passwd\n" }
    }
    expect "$ "
    send "rm ${remote_path}/${jar_name} \n"
    expect "$ "
    send "exit\n"
	expect eof
EOF
cd ..
chmod 644 ${local_path}/${jar_name}
echo `pwd`

#scp -P ${port} ${local_path}/${jar_name} ${uname}@${host}:${remote_path}
# copy jar to remote host
#bash -c 解析 "*" 通配符 

expect  << EOF
		set timeout 3600
		spawn bash -c  "scp -P ${port} ${local_path}/${jar_name} ${uname}@${host}:${remote_path}"
		expect {
			"*yes/no" { send "yes\n"; exp_continue }
			"*password:" { send "$passwd\n" }
		}
		expect "$ "
		expect eof
EOF


# restart remote host service
expect << EOF
    set timeout 3600
    spawn ssh -p ${port} ${uname}@${host}
    expect {
		"*yes/no" { send "yes\n"; exp_continue }
		"*password:" { send "$passwd\n" }
    }
    expect "$ "
    send "cd ${remote_path} && ../deploy.sh \n"
    expect "$ "
    send "exit\n"
    expect eof
EOF


